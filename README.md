# Minha build do sfm

![](./screenshot.png)

## Patches

- Adiciona dois argumentos opcionais que especificam localização inicial dos painéis esquerdo/direito ([dirargs](https://github.com/afify/sfm-patches/blob/main/sfm-dirargs-0.3.1.diff))
