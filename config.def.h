/* See LICENSE file for copyright and license details.*/

#ifndef CONFIG_H
#define CONFIG_H

/* colors                      fg,  bg */
static const Cpair cdir    = { 27, 0 };
static const Cpair cfile   = { 15, 0 };
static const Cpair clnk    = { 33, 0 };
static const Cpair cblk    = { 95, 0 };
static const Cpair cchr    = { 94, 0 };
static const Cpair cifo    = { 15, 0 };
static const Cpair csock   = { 92, 0 };
static const Cpair cexec   = { 28, 0 };
static const Cpair cother  = { 90, 0 };

static const Cpair cframe  = { 0, 0 };
static const Cpair cpanell = { 33,  0 };
static const Cpair cpanelr = { 33,  0 };
static const Cpair cerr    = { 9, 0 };
static const Cpair cprompt = { 28,  0 };
static const Cpair csearch = { 255, 0 };
static const Cpair cstatus = { 7, 0 };

/* commands */
#if defined(__linux__)
#define CHFLAG "chattr"
#else
#define CHFLAG "chflags"
#endif
static const char *rm_cmd[]      = { "rm", "-rf" }; /* delete */
static const char *cp_cmd[]      = { "cp", "-r" }; /* copy */
static const char *chown_cmd[]   = { "chown", "-R" }; /* change file owner and group */
static const char *chmod_cmd[]   = { "chmod" }; /* change file mode bits */
static const char *chflags_cmd[] = { CHFLAG }; /* change file flags */
static const char *mv_cmd[]      = { "mv" }; /* move */
static const char delconf[]      = "y"; /* Confirm deletion with */

static const size_t rm_cmd_len      = LEN(rm_cmd);
static const size_t cp_cmd_len      = LEN(cp_cmd);
static const size_t chown_cmd_len   = LEN(chown_cmd);
static const size_t chmod_cmd_len   = LEN(chmod_cmd);
static const size_t chflags_cmd_len = LEN(chflags_cmd);
static const size_t mv_cmd_len      = LEN(mv_cmd);
static const size_t delconf_len     = LEN(delconf);

/* bookmarks */
static const char root[]   = "/";

/* software */
static const char *mpv[]         = { "mpv" };
static const char *nsxiv[]       = { "nsxiv", "." };
static const char *nvim[]        = { "nvim" };
static const char *emacs[]       = { "emacs" };
static const char *mpv_audio[]   = { "mpv", "--audio-display=no" };
static const char *zathura[]     = { "zathura" };
static const char *libreoffice[] = { "libreoffice" };

/* extensions*/
static const char *videos[]    = { "avi", "flv", "wav", "webm", "m2v", "m4a", "m4v", "mkv", "mov", "mp4", "mpeg", "mpg" };
static const char *images[]    = { "bmp", "jpg", "jpeg", "png", "gif", "xpm", "png" };
static const char *text[]      = { "txt", "1", "c", "h", "mk", "md", "sh", "desktop" };
static const char *org[]       = { "org" };
static const char *audio[]     = { "mp3", "flac", "wmv", "wma" };
static const char *pdf[]       = { "epub", "pdf" };
static const char *documents[] = { "odt", "doc", "docx", "xls", "xlsx", "odp", "ods", "pptx", "odg" };

static Rule rules[] = {
	{videos,    LEN(videos),    mpv,         LEN(mpv)         },
	{images,    LEN(images),    nsxiv,       LEN(nsxiv)       },
	{text,      LEN(text),      nvim,        LEN(nvim)        },
	{org,       LEN(org),       emacs,       LEN(emacs)       },
	{audio,     LEN(audio),     mpv_audio,   LEN(mpv_audio)   },
	{pdf,       LEN(pdf),       zathura,     LEN(zathura)     },
	{documents, LEN(documents), libreoffice, LEN(libreoffice) },
};

/* normal keys */
static Key nkeys[] = {
	/* keyval                      function      arg */
	{ {.key = TB_KEY_ARROW_DOWN},  mv_ver,       {.i = -1}       },
	{ {.key = TB_KEY_ARROW_UP},    mv_ver,       {.i = +1}       },
	{ {.key = TB_KEY_CTRL_U},      mv_ver,       {.i = +3}       },
	{ {.key = TB_KEY_CTRL_D},      mv_ver,       {.i = -3}       },
	{ {.key = TB_KEY_ARROW_RIGHT}, mvfwd,        {.i = 0}        },
	{ {.key = TB_KEY_ARROW_LEFT},  mvbk,         {.i = 0}        },
	{ {.ch = 'g'},                 mvtop,        {.i = 0}        },
	{ {.ch = 'G'},                 mvbtm,        {.i = 0}        },
	{ {.ch = 'n'},                 crnf,         {0}             },
	{ {.ch = 'N'},                 crnd,         {0}             },
	{ {.ch = 'd'},                 delent,       {0}             },
	{ {.ch = 'D'},                 dupl,         {0}             },
	{ {.ch = 'x'},                 calcdir,      {0}             },
	{ {.ch = '/'},                 start_filter, {0}             },
	{ {.ch = 'q'},                 quit,         {0}             },
	{ {.ch = 'v'},                 start_vmode,  {0}             },
	{ {.ch = 'y'},                 yank,         {0}             },
	{ {.ch = 'p'},                 paste,        {0}             },
	{ {.ch = 'P'},                 selmv,        {0}             },
	{ {.ch = 'c'},                 start_change, {0}             }, // Multiple keys
	{ {.ch = 's'},                 opnsh,        {0}             },
	{ {.key = TB_KEY_TAB},         switch_pane,  {0}             },
	{ {.ch = 'r'},                 refresh,      {0}             },
	{ {.ch = '\\'},                bkmrk,        {.v = root}     },
	{ {.ch = 'h'},                 toggle_df,    {0}             },
};

/* change keys */
static Key ckeys[] = {
	/* keyval                      function         arg */
	{ {.ch = 'w'},                 rname,            {0}            },
	{ {.ch = 'o'},                 chngo,            {0}            },
	{ {.ch = 'm'},                 chngm,            {0}            },
	{ {.ch = 'f'},                 chngf,            {0}            },
	{ {.ch = 'q'},                 exit_change,      {0}            },
	{ {.ch = 'c'},                 exit_change,      {0}            },
	{ {.key = TB_KEY_ESC},         exit_change,      {0}            },
};

/* visual keys */
static Key vkeys[] = {
	/* keyval                      function         arg */
	{ {.ch = 'j'},                 seldwn,          {.i = -1}      },
	{ {.key = TB_KEY_ARROW_DOWN},  seldwn,          {.i = -1}      },
	{ {.ch = 'k'},                 selup,           {.i = +1}      },
	{ {.key = TB_KEY_ARROW_UP},    selup,           {.i = +1}      },
	{ {.ch = 'a'},                 selall,          {0}            },
	{ {.ch = 'y'},                 selynk,          {0}            },
	{ {.ch = 'd'},                 seldel,          {.v = delconf} },
	{ {.ch = 'q'},                 exit_vmode,      {0}            },
	{ {.ch = 'v'},                 exit_vmode,      {0}            },
	{ {.key = TB_KEY_ESC},         exit_vmode,      {0}            },
};

static const size_t nkeyslen = LEN(nkeys);
static const size_t vkeyslen = LEN(vkeys);
static const size_t ckeyslen = LEN(ckeys);

/* permissions */
static const mode_t ndir_perm = S_IRWXU;
static const mode_t nf_perm   = S_IRUSR | S_IWUSR;

/* dotfiles */
static int show_dotfiles = 1;

/* statusbar */
static const char dtfmt[] = "%a %d/%m/%Y - %H:%M"; /* date time format */

/* unicode chars */
#define u_hl  0x2500 /* ─ */
#define u_vl  0x2502 /* │ */
#define u_cnw 0x250C /* ┌ */
#define u_cne 0x2510 /* ┐ */
#define u_csw 0x2514 /* └ */
#define u_cse 0x2518 /* ┘ */
#define u_mn  0x252C /* ┬ */
#define u_ms  0x2534 /* ┴ */

#endif /* CONFIG_H */

